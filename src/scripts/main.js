function getRandom(min, max){
  return Math.random() * (max - min) + min;
}

var isSafari = /constructor/i.test(window.HTMLElement);
var isFF = !!navigator.userAgent.match(/firefox/i);

if (isSafari) {
  document.getElementsByTagName('html')[0].classList.add('safari');
}

// Remove click on button for demo purpose
Array.prototype.slice.call(document.querySelectorAll('.button'), 0).forEach(function(bt) {
  bt.addEventListener('click', function(e) {
    e.preventDefault();
  });
});

initBt2();
initBt1();
initBt3();

// Button 1
function initBt1() {
  var bt = document.querySelectorAll('.component-1')[0];
  var filter = document.querySelectorAll('#filter-goo-2 feGaussianBlur')[0];
  var particleCount = 12;
  var colors = ['#DE8AA0', '#8AAEDE', '#FFB300', '#60C7DA']
  var eventList = ['mouseenter', 'click'];
  for (event of eventList) {
    bt.addEventListener(event, function() {
      var particles = [];
      var tl = new TimelineLite({onUpdate: function() {
        filter.setAttribute('x', 0);
      }});

      for (var i = 0; i < particleCount; i++) {
        particles.push(document.createElement('span'));
        bt.appendChild(particles[i]);

        particles[i].classList.add(i % 2 ? 'left' : 'right');

        var dir = i % 2 ? '-' : '+';
        var r = i % 2 ? getRandom(-1, 1)*i/2 : getRandom(-1, 1)*i;
        var size = i < 2 ? 1 : getRandom(0.4, 0.8);
        var tl = new TimelineLite({ onComplete: function(i) {
          particles[i].parentNode.removeChild(particles[i]);
          this.kill();
        }, onCompleteParams: [i] });

        tl.set(particles[i], { scale: size });
        tl.to(particles[i], 0.1, { x: dir + 20, scaleX: 3, ease: SlowMo.ease.config(0.1, 0.7, false) });
        tl.to(particles[i], 0.1, { scale: size, x: dir +'=25' }, '-=0.1');
        if(i >= 2) tl.set(particles[i], { backgroundColor: colors[Math.round(getRandom(0, 3))] });
        tl.to(particles[i], 0.6, { x: dir + getRandom(60, 100), y: r*10, scale: 0.1, ease: Power3.easeOut });
        tl.to(particles[i], 0.2, { opacity: 0, ease: Power3.easeOut }, '-=0.1');
      }
    });
  }
}

// Button 2
function initBt2() {
  var bt = document.querySelectorAll('.component-2')[0];
  var filter = document.querySelectorAll('#filter-goo-2 feGaussianBlur')[0];
  var particleCount = 12;
  var colors = ['#DE8AA0', '#8AAEDE', '#FFB300', '#60C7DA']
  var eventList = ['mouseenter', 'click'];
  for (event of eventList) {
    bt.addEventListener(event, function() {
      var particles = [];
      var tl = new TimelineLite({onUpdate: function() {
        filter.setAttribute('x', 0);
      }});

      for (var i = 0; i < particleCount; i++) {
        particles.push(document.createElement('span'));
        bt.appendChild(particles[i]);

        particles[i].classList.add(i % 2 ? 'left' : 'right');

        var dir = i % 2 ? '-' : '+';
        var r = i % 2 ? getRandom(-1, 1)*i/2 : getRandom(-1, 1)*i;
        var size = i < 2 ? 1 : getRandom(0.4, 0.8);
        var tl = new TimelineLite({ onComplete: function(i) {
          particles[i].parentNode.removeChild(particles[i]);
          this.kill();
        }, onCompleteParams: [i] });

        tl.set(particles[i], { scale: size });
        tl.to(particles[i], 0.1, { x: dir + 20, scaleX: 3, ease: SlowMo.ease.config(0.1, 0.7, false) });
        tl.to(particles[i], 0.1, { scale: size, x: dir +'=25' }, '-=0.1');
        if(i >= 2) tl.set(particles[i], { backgroundColor: colors[Math.round(getRandom(0, 3))] });
        tl.to(particles[i], 0.6, { x: dir + getRandom(60, 100), y: r*10, scale: 0.1, ease: Power3.easeOut });
        tl.to(particles[i], 0.2, { opacity: 0, ease: Power3.easeOut }, '-=0.1');
      }
    });
  }
}

// Button 3
function initBt3() {
  var bt = document.querySelectorAll('.component-3')[0];
  var filter = document.querySelectorAll('#filter-goo-2 feGaussianBlur')[0];
  var particleCount = 12;
  var colors = ['#DE8AA0', '#8AAEDE', '#FFB300', '#60C7DA']
  var eventList = ['mouseenter', 'click'];
  var test = function () {
    var particles = [];
    var tl = new TimelineLite({onUpdate: function() {
      filter.setAttribute('x', 0);
    }});


    for (var i = 0; i < particleCount; i++) {
      particles.push(document.createElement('span'));
      bt.appendChild(particles[i]);

      particles[i].classList.add(i % 2 ? 'left' : 'right');

      var dir = i % 2 ? '-' : '+';
      var r = i % 2 ? getRandom(-1, 1)*i/2 : getRandom(-1, 1)*i;
      var size = i < 2 ? 1 : getRandom(0.4, 0.8);
      var tl = new TimelineLite({ onComplete: function(i) {
        particles[i].parentNode.removeChild(particles[i]);
        this.kill();
      }, onCompleteParams: [i] });

      tl.set(particles[i], { scale: size });
      tl.to(particles[i], 0.1, { x: dir + 20, scaleX: 3, ease: SlowMo.ease.config(0.1, 0.7, false) });
      tl.to(particles[i], 0.1, { scale: size, x: dir +'=25' }, '-=0.1');
      if(i >= 2) tl.set(particles[i], { backgroundColor: colors[Math.round(getRandom(0, 3))] });
      tl.to(particles[i], 0.6, { x: dir + getRandom(60, 100), y: r*10, scale: 0.1, ease: Power3.easeOut });
      tl.to(particles[i], 0.2, { opacity: 0, ease: Power3.easeOut }, '-=0.1');
    }
  }
  for (event of eventList) {
    bt.addEventListener(event, test);
  }
}

$(function() {
  $('a[href*="#"]:not([href="#"])').click(function(e) {
    e.preventDefault();
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 10);
        return false;
      }
    }
  });
});

$( '.swipebox-video' ).swipebox({autoplayVideos: true});
$( '.swipebox' ).swipebox({
  hideBarsDelay: 300000,
  loopAtEnd: true
});

$(function(){
  $(document.body)
    .on('click touchend','#swipebox-slider .current img', function(e){
      return false;
    })
    .on('click touchend','#swipebox-slider .current', function(e){
      $('#swipebox-close').trigger('click');
    });
});

ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
            center: [59.9669353,30.3095406],
            zoom: 15
        }, {
            searchControlProvider: 'yandex#search'
        }),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'ДК им. Ленсовета',
            balloonContent: 'ДК им. Ленсовета',
            iconCaption: 'ДК им. Ленсовета'
        }, {
            preset: 'islands#redDotIconWithCaption'
        });

    myMap.geoObjects.add(myPlacemark);
    myMap.behaviors.disable('scrollZoom');
});

$('.header-mobile__menu').on('click', function() {
  $('.header-mobile').show();
});

$('.header-mobile__close, .header-mobile a').on('click', function() {
  $('.header-mobile').hide();
});

(function(){
  var parallax = document.querySelectorAll(".butterfly_one"),
      butterfly2 = document.querySelectorAll('.butterfly_two'),
      butterfly3 = document.querySelectorAll('.butterfly_three'),
      butterfly4 = document.querySelectorAll('.butterfly_four')
      speed = 0.5;

  window.onscroll = function(){
    [].slice.call(parallax).forEach(function(el,i, id){
      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "" + (windowYOffset * speed / 30) + "em";

      el.style.bottom = elBackgrounPos;
    });

    [].slice.call(butterfly2).forEach(function(el,i){

      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "" + (windowYOffset * speed / 45) + "em";

      el.style.bottom = elBackgrounPos;
    });

    [].slice.call(butterfly3).forEach(function(el,i){

      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "" + (windowYOffset * speed / 25) + "em";

      el.style.bottom = elBackgrounPos;
    });

    [].slice.call(butterfly4).forEach(function(el,i){

      var windowYOffset = window.pageYOffset,
          elBackgrounPos = "" + (windowYOffset * speed / 65) + "em";

      el.style.bottom = elBackgrounPos;
    });
  };
})();

(function() {
  var $slick_slider = $('.photos-slider');
  var settings = {
    centerMode: true,
    arrows: false
  };

  if ($(window).width() < 768) {
    $slick_slider.slick(settings);
  }
}());
